var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var TodoConstants = require('../constants/TodoConstants');
var assign = require('object-assign');

var CHANGE_EVENT = 'change';

var _todos = {};

function create(text) {
    var id = new Date().toString();
    _todos[id] = {
	'id': id,
	'complete': false,
	'text': text
    };
}

function update(id, updates) {
    _todos[id] = assign({},_todos[id], updates);
}

function updateAll(updates) {
    for(var id in _todos) {
	update(id, updates)
    }
}

function destroyCompleted() {
    for (var id in _todos) {
	if(_todos[id].complete) {
	    destroy(id);
	}
    }
}

function destroy(id) {
    delete _todos[id];
}

var TodoStore = assign({}, EventEmitter.prototype, {
    areAllComplete: function() {
	for(var id in _todos) {
	    if(!_todos[id].complete) {
		return false;
	    }
	}
	return true;
    },
    getAll: function() {
	return _todos;
    },
    emitChange: function() {
	this.emit(CHANGE_EVENT);
    },
    addChangeEventListener: function(callback) {
	this.on(CHANGE_EVENT, callback);
    },
    removeChangeEventListener: function(callback) {
	this.removeListener(CHANGE_EVENT, callback);
    },
    dispatcherIndex: AppDispatcher.register(function(payload){
	var action = payload.action;
	var text;

	switch(action.actionType){
	    case TodoConstants.TODO_CREATE:
	    text = action.text.trim();
	    if(text !== '') {
		create(text);
		TodoStore.emitChange();
	    }
	    break;
	    case TodoConstants.TODO_DESTROY:
	    destroy(action.id);
	    TodoStore.emitChange();
	    break;
	    default:
	    break;
	}
	return true;
    })
});

module.exports = TodoStore;
